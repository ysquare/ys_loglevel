<?php

namespace YSquare\Loglevel\Core;

class WidgetControl extends WidgetControl_parent {
    /**
     * Returns error reporting level.
     * Returns disabled error logging if server is misconfigured #2015 E_NONE replaced with 0.
     *
     * @return int
     * @deprecated underscore prefix violates PSR12, will be renamed to "getErrorReportingLevel" in next major
     */
    protected function _getErrorReportingLevel() // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        $logLevelHelper = oxNew(LoglevelHelper::class);
        $errorReporting = $logLevelHelper->getErrorReportingLevel();

        if(false===$errorReporting) {
            $errorReporting = parent::_getErrorReportingLevel();
        }
        return $errorReporting;
    }

    protected function getErrorReportingLevel()
    {
        // Copy upper code as soon as the deprecation machine kicked the upper function...
    }
}
