<?php

namespace YSquare\Loglevel\Core;

use OxidEsales\Eshop\Core\Registry;

class LoglevelHelper  {
    protected $prepLoglevels = [
        'default' => E_ALL & ~E_NOTICE,
        'all' => E_ALL,
        'all_no_notice' => E_ALL & ~E_NOTICE,
        'all_no_noticewarning'=> E_ALL & ~E_NOTICE & ~E_WARNING,
        'just_errors' => E_ERROR,
    ];

    public function getErrorReportingLevel() {
        $config = Registry::getConfig();
        $override=$config->getConfigParam('blYSquareLoglevelOverride');
        $logLevelOverride = $config->getConfigParam('YSquareLoglevelOverrideDefaults');

        if(ini_get('log_errors')) {
            if($override) {
                if($logLevelOverride == "flexible") {
                    return $config->getConfigParam('YSquareLoglevelOverrideProValue');
                } else {
                    return $this->prepLoglevels[$logLevelOverride];
                }
            }
        }

        return false;
    }

}