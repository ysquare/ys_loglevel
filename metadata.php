<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.1';

$ys_prequel = '<style>.listitemfloating .ys-logo {display:inline-block; width:15px; height:15px; line-height: 16px; text-align: center; background-color: #000; color:#FFF;} h1 .ys-logo {display:inline-block; width:31px; height:31px; line-height: 31px; text-align: center; background-color: #000; color:#FFF;}</style><span class="ys-logo">Y</span> <b>[Y-SQUARE]</b> ';

/**
 * Module information
 */
$aModule = [
    'id'          => 'ys_loglevel',
    'title'       => $ys_prequel . 'Log Level Configurator',
    'description' => [
        'de' => 'Macht das Log-Level konfigurierbar',
        'en' => '',
    ],
    'thumbnail'   => 'logo.png',
    'version'     => '1.0.1',
    'author'      => 'Y-SQUARE',
    'url'         => 'http://www.y-square.com',
    'email'       => 'info@y-square.com',
    'extend'      => [
        \OxidEsales\Eshop\Core\ShopControl::class   => \YSquare\Loglevel\Core\ShopControl::class,
        \OxidEsales\Eshop\Core\WidgetControl::class => \YSquare\Loglevel\Core\WidgetControl::class,
    ],
    'controllers' => [],
    'templates'   => [],
    'blocks'      => [],
    'settings'    => [
        [
            'group' => 'ys_loglevel_settings',
            'name'  => 'blYSquareLoglevelOverride',
            'type'  => 'bool',
            'value' => 'false',
        ],
        [
            'group'       => 'ys_loglevel_settings',
            'name'        => 'YSquareLoglevelOverrideDefaults',
            'type'        => 'select',
            'value'       => 'default',
            'constraints' => 'default|all|all_no_notice|all_no_noticewarning|just_errors|flexible',
        ],
        [
            'group' => 'ys_loglevel_settings',
            'name'  => 'YSquareLoglevelOverrideProValue',
            'type'  => 'str',
            'value' => '',
        ],
    ],
    'events'      => [],
];