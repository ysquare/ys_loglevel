<?php
/**
 * This file is part of OXID eSales GDPR opt-in module.
 *
 * OXID eSales GDPR opt-in module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales GDPR opt-in module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales GDPR opt-in module.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2018
 */

$sLangName = 'Deutsch';

$aLang = [
    'charset'                                              => 'UTF-8',
    'SHOP_MODULE_GROUP_ys_loglevel_settings' => 'Einstellungen',
    'SHOP_MODULE_blYSquareLoglevelOverride' => 'Log Level übersteuern',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults' => 'Log Level Optionen',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_default' => 'OXID Standard',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_all' => 'Alles',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_all_no_notice' => 'Alles außer "notice"',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_all_no_noticewarning' => 'Alles außer "notice" & "warning"',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_just_errors' => 'Nur Fehler (ERROR)',
    'SHOP_MODULE_YSquareLoglevelOverrideDefaults_flexible' => 'Hey! Ich weiß was ich tue!',
    'SHOP_MODULE_YSquareLoglevelOverrideProValue' => 'Du weißt was Du tust? Dann mal los...',
];